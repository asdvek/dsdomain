import random
import dsdictionary
import dsdivider

class game:
    # shared list of running game session ids
    session_ids = []

    def __init__(self):
        self.id = self._generate_id()
        self.fragments = None
        self.correct_word = None
        self.started = False
        self.player_count = 0
        self.fragments = None
        game.session_ids.append(self.id)

    def start(self):
        print(f"Starting game: {self.id}")
        self.correct_word, self.fragments = dsdivider.get_word2(self.player_count, dsdictionary.words())
        self.started = True
        print("Correct word: {}".format(self.correct_word))
        return self.correct_word
    
    def is_started(self):
        return self.started
    
    def get_segment(self):
        return self.fragments.pop()
    
    def join(self):
        if self.started is False:
            self.player_count += 1
            print(f"A new player joined the game. Total players: {self.player_count}")
    
    def check(self, guessed_word):
        return guessed_word.lower() == self.correct_word.lower()
    
    @classmethod
    def does_session_exist(cls, session_id):
        for sid in cls.session_ids:
            if sid == session_id:
                return True
        return False
    
    # object string representation
    def __repr__(self):
        return f"game[{self.id}]"
        
    def _generate_id(self):
        id_candidate = random.randint(0, 9999)
        while id_candidate in game.session_ids:
            id_candidate = random.randint(0, 9999)
        return id_candidate
