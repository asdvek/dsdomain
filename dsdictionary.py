import json

def words():
    with open('dsdictionary.csv', 'r') as fd:
        lines = [line.strip() for line in fd.readlines()]
        return lines

def as_json_str():
    return json.dumps(words())
