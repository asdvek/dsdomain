from flask import Flask, send_from_directory, jsonify, request
from dsdictionary import words
from game import game


app = Flask(__name__, static_folder='./build', static_url_path='/')

# dictionary of game sessions
#   id -> game object
games = dict()

# Front end navigation
@app.route('/')
@app.route('/dictionary')
@app.route('/secretadminpage')
@app.route('/dsdecryption')
@app.route('/hint')
@app.route('/bomb')
def index():
    response = send_from_directory('./build', 'index.html')
    return response

# API routing
@app.route('/api/test')
def test():
    return "Toimii 😎"

### DSDictionary ###
@app.route('/api/dictionary/words')
def dsdictionary_words_json():
    return jsonify(words())

### DSDecryption ###
@app.route('/api/dsdecryption/create', methods=['POST'])
def dsdecryption_create():
    print("POST request to /api/dsdecryption/create")
    new_game = game()
    games[new_game.id] = new_game
    print("games after creating a new session\n{}".format(games))
    return {"id": new_game.id}

# start game with session id 'game_id'
@app.route('/api/dsdecryption/start/<int:game_id>', methods=['POST'])
def dsdecryption_start(game_id):
    print(f"POST request to /api/dsdecryption/start/{game_id}")
    word = games[game_id].start()
    return {"word": word}

# check if the guessed word in session 'game_id' was correct
@app.route('/api/dsdecryption/check/<int:game_id>', methods=['POST'])
def dsdecryption_check(game_id):
    print(f"POST request to /api/dsdecryption/check/{game_id}")
    guessed_word = request.json['word']
    print("  guessed word {}".format(guessed_word))
    result = games[game_id].check(guessed_word)
    return {"result": result}

# join game session from fuksi client
@app.route('/api/dsdecryption/join/<int:game_id>', methods=['POST'])
def dsdecryption_join(game_id):
    print(f"POST request to /api/dsdecryption/join/{game_id}")
    entered_session_id = int(request.json['session_id'])
    if (game.does_session_exist(entered_session_id) and not games[entered_session_id].is_started()):
        games[entered_session_id].join()
        return {"result": True}
    else:
        return {"result": False}

# fetch DSDictionary word fragment
@app.route('/api/dsdecryption/getword/<int:game_id>', methods=['POST'])
def dsdecryption_getword(game_id):
    print(f"POST request to /api/dsdecryption/getword/{game_id}")

    if (games[game_id].is_started()):
        segment = games[game_id].get_segment()
        return {"result": True, "wordSegment": segment}
    else:
        return {"result": False}
