import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App.js';
import NoSleep from 'nosleep.js';

var noSleep = new NoSleep();
// Enable wake lock.
// (must be wrapped in a user input event handler e.g. a mouse or touch handler)
document.addEventListener('click', function enableNoSleep() {
  document.removeEventListener('click', enableNoSleep, false);
  noSleep.enable();
}, false);

ReactDOM.render(
	<App />, document.getElementById('root')
);
