import React from 'react';

const Hint = () => {
    return (
        <div className='container'>
            <iframe title="location" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d495.94143462656336!2d24.82948982924439!3d60.18460966567961!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zNjDCsDExJzA0LjYiTiAyNMKwNDknNDguMSJF!5e0!3m2!1sen!2sfi!4v1600706040403!5m2!1sen!2sfi" width="600" height="450" frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </div>
    )
}

export default Hint;