import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Dictionary = () => {
    const [words, setWords] = useState([])
    const [search, setSearch] = useState('')

    useEffect(() => {
        console.log('effect')
        axios
            .get('/api/dictionary/words')
            .then(response => {
                console.log(response.data)
                setWords(response.data)
            })
    }, [])

    const handleSearchChange = (event) => {
        setSearch(event.target.value)
    }

    return (
        <div id="dictionary">
            <div className="container">
                <h1 id="title">DSDictionary</h1>
                <label>Search</label>
                <form>
                    <input value={search} onChange={handleSearchChange} className='inputfield' />
                </form>
                <ul>
                    {
                        words
                            .filter(word => word.toLowerCase().includes(search.toLowerCase()))
                            .map(word => <li>{word}</li>)
                    }
                </ul>
            </div>
        </div>
    )
}


export default Dictionary;
