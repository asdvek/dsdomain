import React, { useState } from 'react';
import axios from 'axios';
import qr from './qr.png'
import qrtroll from './qr-troll.png'

const DecryptionAdmin = () => {
    const [id, setId] = useState('')                        // game session id
    const [word, setWord] = useState('')                    // correct DSDictionary word to be decrypted (not even necessary since the check is server side)
    const [inputWord, setInputWord] = useState('')          // text input field value
    const [wordResult, setWordResult] = useState()          // was the typed guess for DSDictionary word correct?

    const onNewSessionClick = (event) => {
        axios
            .post('/api/dsdecryption/create')
            .then(response => {
                console.log(response.data)
                setId(response.data.id)
            })
    }
    const onStartSessionClick = (event) => {
        console.log("id", id, id.toString().length)
        if (!id.toString().length > 0) {
            console.log("No game session has been created. Missing session id.")
            return
        }
        axios
            .post('/api/dsdecryption/start/' + id.toString())
            .then(response => {
                console.log(response.data)
                setWord(response.data.word)
            })
    }
    const handleInputChange = (event) => {
        console.log(event.target.value)
        setInputWord(event.target.value)
    }

    return (
        <div className='container'>
            <button onClick={onNewSessionClick}>New Session</button>
            <QrCode image_path={qr} />
            <IdDisplay id={id} />
            <button onClick={onStartSessionClick}>Start Session</button>
            <WordInput
                gameId={id} inputWord={inputWord}
                handleInputChange={handleInputChange} setWordResult={setWordResult} />
            <VictoryMessage wordResult={wordResult} />
        </div>
    )
}

const VictoryMessage = ({ wordResult }) => {
    console.log("VictoryMessage", wordResult)
    if (wordResult === true) {
        return (<><h1>ACCESS GRANTED!</h1>
            <h2>SCAN CODE TO ACTIVATE BOMB</h2>
            <QrCode image_path={qrtroll} /></>)
    }
    else if (wordResult === false) {
        return (<h1 className="red-text">ACCESS DENIED!</h1>)
    }
    else {
        return <></>
    }
}

const WordInput = ({ gameId, inputWord, handleInputChange, setWordResult }) => {
    const checkInputWord = (event) => {
        event.preventDefault()
        console.log('button clicked', event.target)
        axios
            .post('/api/dsdecryption/check/' + gameId.toString(), { "word": inputWord })
            .then(response => {
                console.log(response.data)
                setWordResult(response.data.result)
            })
    }

    return (
        <form onSubmit={checkInputWord}>
            <label>Enter password: </label>
            <input value={inputWord} onChange={handleInputChange} className='inputfield'></input>
            <button type="submit" className='buttton'>execute!</button>
        </form>
    )
}

const IdDisplay = ({ id }) => {
    return (
        <h1>Session ID: {id}</h1>
    )
}

const QrCode = ({ image_path }) => {
    return (
        <img src={image_path} alt="link broken :(" id='qrcode'></img>
    )
}

export default DecryptionAdmin;