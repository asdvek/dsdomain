import React, { useState, useRef, useEffect } from 'react';
import axios from 'axios';

const Fuksi = () => {
    const [sessionInputVisible, setSessionInputVisible] = useState(true)                // is the session input field visible
    const [fetchSegmentButtonVisible, setFetchSegmentButtonVisible] = useState(true)
    const [inputSessionId, setInputSessionId] = useState('')                            // session id input field
    const [wordSegmentText, setWordSegmentText] = useState('')

    const handleInputChange = (event) => {
        console.log(event.target.value)
        setInputSessionId(event.target.value)
    }

    return (
        <div className='container'>
            {
                sessionInputVisible
                    ? <SessionIdInput
                        inputSessionId={inputSessionId}
                        handleInputChange={handleInputChange}
                        setSessionInputVisible={setSessionInputVisible} />
                    : null
            }
            {
                sessionInputVisible
                    ? null
                    : <FetchSegmentButton
                        inputSessionId={inputSessionId}
                        fetchSegmentButtonVisible={fetchSegmentButtonVisible}
                        setFetchSegmentButtonVisible={setFetchSegmentButtonVisible}
                        setWordSegmentText={setWordSegmentText}
                    />
            }
            <WordSegment wordSegmentText={wordSegmentText} />
        </div>
    )
}

const SessionIdInput = ({ inputSessionId, handleInputChange, setSessionInputVisible }) => {
    const joinGameSession = (event) => {
        event.preventDefault()
        console.log('button clicked', event.target)
        axios
            .post('/api/dsdecryption/join/' + inputSessionId, { "session_id": inputSessionId })
            .then(response => {
                console.log(response.data)
                // hide session id input form
                // TODO: Display error message if joining to session fails
                setSessionInputVisible(!response.data.result)
            })
    }

    return (
        <form onSubmit={joinGameSession}>
            <label>Enter Session ID:</label>
            <input type="number" value={inputSessionId} onChange={handleInputChange} className='inputfield' />
            <button type="submit">execute!</button>
        </form>
    )
}

const messages = ["Hold your horses!", "Not so fast there buddy!", "What's the hurry?", "Hacking button has not yet been activated."]
const FetchSegmentButton = ({ inputSessionId, fetchSegmentButtonVisible, setFetchSegmentButtonVisible, setWordSegmentText }) => {

    const fetchSegment = (event) => {
        console.log("Fetching word segment")
        axios
            .post('/api/dsdecryption/getword/' + inputSessionId)
            .then(response => {
                console.log(response.data)
                if (response.data.result === true) {
                    setFetchSegmentButtonVisible(false)
                    setWordSegmentText(response.data.wordSegment)
                }
                else {
                    alert(messages[Math.floor(Math.random()*messages.length)]+" Wait for your teammates to join.");
                    console.log("Fuck you the game hasn't started yet :)")
                }
            })
    }

    return (
        <>
            {
                fetchSegmentButtonVisible
                    ? <button onClick={fetchSegment}>Start hack!</button>
                    : null
            }
        </>
    )
}

const WordSegment = ({ wordSegmentText }) => {
    return (
        <div id='segment'>
            {wordSegmentText}
        </div>
    )
}

export default Fuksi;
