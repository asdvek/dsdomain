import React from 'react';
import { redirect, Route } from 'react-router-dom'

const Troll = () => {


    const [counter, setCounter] = React.useState(15);
    React.useEffect(() => {
        counter > 0 && setTimeout(() => setCounter(counter - 1), 1000);
    }, [counter])

    if (counter == 0) {
        window.location.href = 'https://www.youtube.com/watch?v=M5V_IXMewl4'
    }

    return (
        <div className='container'>
            <h1>BOMB TIMER IS SET</h1>
            <h1>{counter}</h1>
        </div>
    )
}

export default Troll;
