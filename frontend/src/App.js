import React, { useRef, useEffect } from 'react';
import {
	BrowserRouter as Router,
	Switch,
	Route
} from "react-router-dom";

import Home from './Home.js';
import Dictionary from './Dictionary.js';
import DecryptionAdmin from './DecryptionAdmin.js'
import Fuksi from './Fuksi.js'
import Hint from './Hint.js'
import Troll from './troll.js'


const App = () => {
	const canvasRef = useRef()

	useEffect(() => {
		// Initialising the canvas
		let canvas = canvasRef.current;
		let ctx = canvas.getContext('2d');

		// Setting the width and height of the canvas
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;

		// Setting up the letters
		let letters = 'DS';
		letters = letters.split('');

		// Setting up the columns
		let fontSize = 10,
			columns = canvas.width / fontSize;

		// Setting up the drops
		let drops = [];
		for (let i = 0; i < columns; i++) {
			drops[i] = 1;
		}

		// Setting up the draw function
		function draw() {
			ctx.fillStyle = 'rgba(0, 0, 0, .1)';
			ctx.fillRect(0, 0, canvas.width, canvas.height);
			for (let i = 0; i < drops.length; i++) {
				let text = letters[Math.floor(Math.random() * letters.length)];
				ctx.fillStyle = '#0f0';
				ctx.fillText(text, i * fontSize, drops[i] * fontSize);
				drops[i]++;
				if (drops[i] * fontSize > canvas.height && Math.random() > .95) {
					drops[i] = 0;
				}
			}
		}

		// Loop the animation
		setInterval(draw, 33);
	})

	return (
		<div>
			<canvas id="hacker_bg" ref={canvasRef}></canvas>
			<Router>
				<Switch>
					<Route path="/dictionary">
						<Dictionary />
					</Route>
					<Route path="/secretadminpage">
						<DecryptionAdmin />
					</Route>
					<Route path="/dsdecryption">
						<Fuksi />
					</Route>
					<Route path="/hint">
						<Hint />
					</Route>
					<Route path="/bomb">
						<Troll />
					</Route>
					<Route path="/">
						<Home />
					</Route>
				</Switch>
			</Router>
		</div>
	)
}


export default App;
