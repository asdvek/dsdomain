import random
import dsdictionary

def fragment_word(fragments,word,f_size,f_nr):
    if len(word) > f_size:
        fragments.append(word[:f_size])
        word = word[f_size:]
        f_nr+=1
        return fragments, word, f_nr
    else:
        fragments.append(word[:f_size])
        f_nr+=1
        return fragments, word, f_nr

def get_word2(n_players, words):
	usable_words = []
	for x in words:
		if len(x) == n_players:
			usable_words.append(x)
	id_of_word = random.randint(0,len(usable_words)-1)
	word = usable_words[id_of_word]
	return_word = word
	fragments = [char for char in return_word]
	random.shuffle(fragments)
	print(return_word,fragments)
	return (return_word, fragments)


def get_word(n_players,words):
	usable_words = []
	for x in words:
		if len(x) >= 2*n_players and len(x) <= 3*n_players:
			usable_words.append(x)
	id_of_word = random.randint(0,len(usable_words)-1)
	word = usable_words[id_of_word]
	return_word = word
	portions_of_word = []
	n_3 = len(word) - 2*n_players
	n_2 = n_players - n_3
	fragments = []
	fragment_nr = 0
	is_both = False
	if n_2 > 0 and n_3 > 0:
		is_both = True
	while is_both:
		option = random.randint(0,1)
		if option == 0:
			fragments,word,fragment_nr = fragment_word(fragments, word,2,fragment_nr)
			n_2 -= 1
		else:
			fragments,word,fragment_nr = fragment_word(fragments, word,3,fragment_nr)
			n_3 -= 1
		if n_3 == 0 or n_2 == 0:
			is_both = False
	if not is_both and n_3 > 0:
		while n_3 > 0:
			fragments,word,fragment_nr = fragment_word(fragments, word,3,fragment_nr)
			n_3 -= 1
	elif not is_both and n_2 > 0:
		while n_2 > 0:
			fragments,word,fragment_nr = fragment_word(fragments, word,2,fragment_nr)
			n_2 -= 1
	
	random.shuffle(fragments)
	print(return_word,fragments)
	return (return_word, fragments)


words = dsdictionary.words()
get_word(4,words)